tabularboonad
=============

its just my wacky xmonad config.  so radical i gave it a name and pretended its a whole new window manager.

wyn
===

ghc, contrib extention thingys n stuff.


wtf
===

quickstart tips.

the main notion of keybindings use:

qwerty wasd direction controls, moved over one space to the right, esdf controls.
mod = tux ( gnu / win / super / whatever you call it )
mod + shift + e/d = moves window through the stack
mod + e/d = moves window focus through the stack (same as mod+tab, and mod+shift+tab)
mod + ctrl + e/s/d/f = merges window into tabs.
mod + ctrl + w = unmerge from tab to solo.
mod + w = move current window to main pane.
mod + g = fullscreen toggle current window.
mod + spacebar = cycle through layout modulation
mod + x/c = move workspace left/right
mod + shift + 1/2/3/4/5/6/7/8/9 = move active window to numbered workspace.
mod + 1/2/3/4/5/6/7/8/9 = move to numbered workspace.
mod + r = run _____ via dmenu (requires dmenu)
F12 = toggle drop-down terminal
mod + F7 = toggle ttyload system load monitor
mod + F8 = toggle htop system monitor
mod + F9 = toggle alsamixer volume control

for other controls, see the config


wut
===

no triple monitor keybind support ootb?!  aw.  (easy enough to add yourself.)


wug
===

rly sweet n serious unobtrusive sensible wm workflow.